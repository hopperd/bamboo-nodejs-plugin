# Node.js Bamboo Plugin

Plugin for integrating [Node.js][node] with [Atlassian Bamboo][bamboo].

[node]: http://nodejs.org/
[bamboo]: http://www.atlassian.com/software/bamboo/overview

## Tasks

* node
* npm

## To do/ideas

* grunt task
* mocha test parsing task
* nodeunit test parsing task

## License

Copyright (C) 2013 - 2013 Atlassian Corporation Pty Ltd. Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.