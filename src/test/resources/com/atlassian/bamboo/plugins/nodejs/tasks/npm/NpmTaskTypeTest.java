package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import junit.framework.TestCase;

public class NpmTaskTypeTest extends TestCase
{
    public void testReplaceLast() throws Exception
    {
        assertEquals("foobarbarbar", NpmTaskType.replaceLast("foobarfoobar", "foo", "bar"));
        assertEquals("barbarbarbar", NpmTaskType.replaceLast("foobarbarbar", "foo", "bar"));
        assertEquals("foobarfoobar", NpmTaskType.replaceLast("foobarfoobar", "faa", "bar"));
    }

    public void testReplaceLastWithWindowsPaths() throws Exception
    {
        String nodePath = "C:\\Users\\foo\\node.exe";
        String npmPath = "C:\\Users\\foo\\npm.cmd";
        assertEquals(npmPath, NpmTaskType.replaceLast(nodePath, "\\node.exe", "\\npm.cmd"));
    }
}
